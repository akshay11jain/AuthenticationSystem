import { Component, OnInit } from '@angular/core';
import { FormGroup , FormControl ,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { RegisterService } from './../register.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  registerForm;
  constructor(private router : Router , private ruser : RegisterService  ) { }



  ngOnInit() {

    this.registerForm = new FormGroup(
      {

        firstname : new FormControl("",Validators.required),
        lastname : new FormControl(""),
        email : new FormControl("",Validators.email),
        username : new FormControl("",Validators.required),
        password : new FormControl("",Validators.required)
      }
      )
     }


  registerUser(user)
  {
    console.log(user);
    this.router.navigate(['registerSuccess']);
  }
  
}
