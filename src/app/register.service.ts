import { Injectable } from '@angular/core';
import { Http,Headers} from '@angular/http';
import 'rxjs/add/operator/map/';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class RegisterService {

  
  private fname;
  private lname;
  private email;
  private username;
  private password;
  constructor(private http: Http ) { }
  
private handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
}

  saveRegisteredUserData(rdata)
  {
     console.log(rdata);  
     
       this.http.post('assets/data.json',rdata, { }).toPromise()
       .then(res => res.json().data)
       .catch(this.handleError);
     }
    }

