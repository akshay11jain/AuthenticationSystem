import { Component, OnInit } from '@angular/core';
import { FormGroup , FormControl ,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from './../user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  loginForm;
  constructor(private router : Router, private loginuser :UserService) { }

  ngOnInit() {
    
        this.loginForm = new FormGroup(
          {
            username : new FormControl("",Validators.required),
            password : new FormControl("",Validators.required)
          }
          )
         }
    
    
    loginUser(user)
      {
        if(user.username =="admin" && user.password =="admin")
          {
            this.loginuser.setUserLoggedIn();
            this.router.navigate(['adminpage']);
          }
        else if(user.username =="user" && user.password =="user")
          {
            this.loginuser.setUserLoggedIn();
            this.router.navigate(['loginSuccess']);
          }
          else 
            {
              console.log("login fail")
            }
      
      }
    

}
