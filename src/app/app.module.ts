import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule , Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegisterFormComponent } from './register-form/register-form.component';
import { RegisterService } from './register.service';
import { UserService } from './user.service';
import { LoginSuccessComponent } from './login-success/login-success.component';
import { AuthguardGuard } from './authguard.guard';
import { RegisterSuccessComponent } from './register-success/register-success.component';
import { AdminpageComponent } from './adminpage/adminpage.component'


const appRoutes : Routes =
[
  {
    path :'',
    component : LoginFormComponent
  },
  {
     path :'register',
     component : RegisterFormComponent
  },
  {
    path :'loginSuccess',
    canActivate:[AuthguardGuard],
    component : LoginSuccessComponent
  },
  {
    path :'registerSuccess',
    component : RegisterSuccessComponent
  },
  {
    path :'adminpage',
    canActivate:[AuthguardGuard],
    component : AdminpageComponent
  }
  
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginFormComponent,
    RegisterFormComponent,
    LoginSuccessComponent,
    RegisterSuccessComponent,
    AdminpageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [RegisterService,UserService,AuthguardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
